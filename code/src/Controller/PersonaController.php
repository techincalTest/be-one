<?php

namespace App\Controller;

use App\Entity\Persona;
use App\Repository\PersonaRepository;
use App\Service\PersonaService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/persona')]
class PersonaController extends AbstractController
{
    #[Route('/', name: 'all_persons')]
    public function index(PersonaRepository $personaRepository): Response
    {
        $persons = $personaRepository->findAll();
        return $this->json([
            'message' => 'All person!',
            'data' => array_map(static function (Persona $person) {
                return [
                    'user' => $person->getNombre(),
                    'fechaNacimiento' => $person->getFechaNacimiento()?->format('d-m-Y')
                ];
            }, $persons),
        ]);
    }

    /**
     * @throws \Exception
     */
    #[Route('/register', name: 'register_person')]
    public function register(Request $request, PersonaService $personaService): \Symfony\Component\HttpFoundation\JsonResponse
    {
        $data = json_decode($request->getContent(), false, 512, JSON_THROW_ON_ERROR);
        return $this->json(['message' => 'Person create successful!', 'person' => $personaService->create($data->nombre, $data->fechaNacimiento)]);
    }

    /**
     * @throws \Exception
     */
    #[Route('/edit/{id}', name: 'edit_person')]
    public function edit(Request $request, $id, PersonaService $personaService): \Symfony\Component\HttpFoundation\JsonResponse
    {
        $data = json_decode($request->getContent(), false, 512, JSON_THROW_ON_ERROR);
        return $this->json(['message' => 'Person edit successful!', 'person' => $personaService->edit($id, $data->nombre, $data->fechaNacimiento)]);
    }
}
