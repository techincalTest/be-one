<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api')]
class HelperController extends AbstractController
{
    /**
     * @throws \Exception
     */
    #[Route('/helper', name: 'app_helper')]
    public function index(Request $request): Response
    {
        $data = json_decode($request->getContent(), false, 512, JSON_THROW_ON_ERROR);
        $arrCadena = explode(' ',$data->cadena);
        $d = [];
        $anterior = '';
        foreach ($arrCadena as $item) {
            if($anterior === 'las'){
                $d[] = $item;
            }
            $anterior = $item;
        }
        $horaInicio = '';
        $horaFin = '';
        $message = 'Operation Fail';

        if(count($d) > 0){
            $horaInicio = $d[0];
            $horaFin = $d[1];
            $message = 'Operation successful';
        }
        return $this->json([
            'message' => $message,
            'data' => ['horaInicio' => $horaInicio, 'horaFin'=>$horaFin],
        ]);
    }
}
