<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Persona;
use App\Repository\PersonaRepository;

class PersonaService
{
    public function __construct(readonly PersonaRepository $personaRepository)
    {
    }

    /**
     * @throws \Exception
     */
    public function create(string $nombre, string $fechaNacimiento): array
    {
        $person = new Persona();
        $person->setNombre($nombre);
        $person->setFechaNacimiento(new \DateTime($fechaNacimiento));
        $this->personaRepository->add($person);
        return $person->toJson();
    }

    /**
     * @throws \Exception
     */
    public function edit(int $id, string $nombre, string $fechaNacimiento): array
    {
        $person = $this->personaRepository->findByIdOrFail($id);
        $person->setNombre($nombre);
        $person->setFechaNacimiento(new \DateTime($fechaNacimiento));
        $this->personaRepository->add($person);
        return $person->toJson();
    }
}