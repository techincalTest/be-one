# Be One

Prueba técnica Be One

## Punto 1 y 2 está en el código

##Punto 3: Pasos para desplegar Proyecto Symfony

- Revisar los requerimientos ( para asegurar que este todo instalado )
- Clonar el repo
- Configurar variables de entorno y configurar el .env
- Instalar o actualizar el vendor (composer install/update)
- Limpiar cache
- Preparar la DDBB
  - Si no esta creada: php bin/console doctrine:database:create
  - Preparar las migraciones: php bin/console make:migration
  - Ejecutar las migraciones: php bin/console doctrine:migrations:migrate
- Si no tenemos configurado el VHOST tendriamos que configurarle y apuntar al index.php (SF > 4)

## Punto 4: Entorno Linux para DDBB
  - Activar y configurar el Firewall para que solo admita peticiones de mi servidor de aplicaciones y solo al puerto 3306 
  - Instalar el gestor de DDBB (Mysq, MariaDB, etc)
  - Crear la base de datos
  - Crear los usuarios
  - Asignar permisos a los usuarios sobre la DDBB

## Punto 5: Git
  - Para ello deberíamos tener una rama que sea para hotfix y que este al mismo nivel que develop
  - Pero en el caso que no este, crearía una rama(hotfix_xxX) partiendo de develop para añadir la corrección, hacer las pruebas y mergear hacia develop

## Punto 6: SQL
    - SELECT dep.nombreDpto, COUNT(*) AS numEmpleados 
      FROM departamentos dep LEFT JOIN empleados em ON dep.id = em.codDepto
      GROUP BY dep.codDepto
      HAVING COUNT(*) >= 3

## Punto 7: jQuery
  - Partiendo de tener el enlace lo aplicaría asi:
    - $(document).ready(function() {
    $('.enlacePdf a').attr('target', '_blank');
    });



### No soy bueno con CSS y no me da tiempo de hacerlo ya que solo programe 2h para esto.


## Punto 10: Sincronizar 
 - No entiendo que es lo que hay que hacer exactamente, sincronizar un saldo, pero donde se almacena?
 